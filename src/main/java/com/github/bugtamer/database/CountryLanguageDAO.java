package com.github.bugtamer.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.github.bugtamer.models.CountryLanguage;


public class CountryLanguageDAO extends DAO {


	// ATTRIBUTES

	private static CountryLanguageDAO singletonCountryLanguageDAO;



	// INSTANTIATION

	private CountryLanguageDAO() throws Exception {
		super();
	}


	public static CountryLanguageDAO getInstance() throws Exception {
		return (singletonCountryLanguageDAO == null) ? new CountryLanguageDAO() : singletonCountryLanguageDAO;
	}



	// SERVICES

	public List<CountryLanguage> getList() {
		return getList("SELECT * FROM countrylanguage ORDER BY Language ASC");
	}


	public List<CountryLanguage> getList(String selectQuery) {
		List<CountryLanguage> cities = new ArrayList<>();
		try {
			Connection conn = DriverManager.getConnection(URL);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(selectQuery);
			CountryLanguage lang;
			while (rs.next()) {
				lang = new CountryLanguage();
				lang.setPercentage(rs.getFloat("Percentage"));
				lang.setCountryCode(rs.getString("CountryCode"));
				lang.setLanguage(rs.getString("Language"));
				lang.setOfficial(parseBoolean(rs.getString("IsOfficial")));
				cities.add(lang);
			}
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return cities;
	}



	// LOW LEVEL IMPLEMENTATION DETAILS

	private boolean parseBoolean(String bool) {
		return bool.equals("T") ? true : false;
	}


}
