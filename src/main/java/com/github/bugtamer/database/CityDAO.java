package com.github.bugtamer.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.github.bugtamer.models.City;
import com.github.bugtamer.models.Country;


public class CityDAO extends DAO {


	// ATTRIBUTES

	private static CityDAO singletonCityDAO;



	// INSTANTIATION

	private CityDAO() throws Exception {
		super();
	}


	public static CityDAO getInstance() throws Exception {
		return (singletonCityDAO == null) ? new CityDAO() : singletonCityDAO;
	}



	// SERVICES

	public City getCityById(String id) {
		City city = null;
		String query = "SELECT * FROM city WHERE Id = ?";
		try {
			Connection conn = DriverManager.getConnection(URL);
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, id);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				city = new City();
				city.setId(rs.getInt("ID"));
				city.setName(rs.getString("Name"));
				city.setCountry(getCountry(rs.getString("CountryCode")));
				city.setDistrict(rs.getString("District"));
				city.setPopulation(rs.getInt("Population"));
			}
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return city;
	}


	public List<City> getList(String selectQuery) {
		List<City> cities = new ArrayList<>();
		try {
			Connection conn = DriverManager.getConnection(URL);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(selectQuery);
			City city;
			while (rs.next()) {
				city = new City();
				city.setId(rs.getInt("ID"));
				city.setName(rs.getString("Name"));
				city.setCountry(getCountry(rs.getString("CountryCode")));
				city.setDistrict(rs.getString("District"));
				city.setPopulation(rs.getInt("Population"));
				cities.add(city);
			}
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return cities;
	}


	public int update(City city) {
		int rowCount = 0;
		if (city != null) {
			String query = "UPDATE city SET Name=?, CountryCode=?, District=?, Population=? WHERE Id=?";
			try {
				Connection conn = DriverManager.getConnection(URL);
				PreparedStatement stmt = conn.prepareStatement(query);
				stmt.setString(1, city.getName());
				stmt.setString(2, city.getCountry().getCode());
				stmt.setString(3, city.getDistrict());
				stmt.setInt   (4, city.getPopulation());
				stmt.setInt   (5, city.getId());
				rowCount = stmt.executeUpdate();
				stmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return rowCount;
	}


	/**
	 * Add a new city
	 *
	 * @param City city
	 * @return index of the city which was added
	 */
	public int add(City city) {
		int index = DAO.NO_ID;
		if (city != null) {
			String query = "INSERT INTO city (Name, CountryCode, District, Population) VALUES (?, ?, ?, ?);";
			try {
				Connection conn = DriverManager.getConnection(URL);
				PreparedStatement stmt = conn.prepareStatement(query);
				stmt.setString(1, city.getName());
				stmt.setString(2, city.getCountry().getCode());
				stmt.setString(3, city.getDistrict());
				stmt.setInt   (4, city.getPopulation());
				System.out.println("add query=" + stmt.toString());
				stmt.executeUpdate();
				index = getIndex(stmt);
				stmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return index;
	}


	public boolean remove(String id) {
		boolean hasError = false;
		if (id != null) {
			String query = "DELETE FROM city WHERE Id = ?";
			try {
				Connection conn = DriverManager.getConnection(URL);
				PreparedStatement stmt = conn.prepareStatement(query);
				stmt.setString(1, id);
				System.out.println("delete query=" + stmt.toString());
				stmt.executeUpdate();
				stmt.close();
				conn.close();
			} catch (SQLException e) {
				hasError = true;
				e.printStackTrace();
			}
		}
		return hasError;
	}



	// LOW LEVEL IMPLEMENTATION DETAILS

	private int getIndex(Statement st) throws SQLException {
		int index = DAO.NO_ID;
		ResultSet rs = st.getGeneratedKeys();
		while(rs.next()) {
			index = rs.getInt(1);
		}
		return index;
	}


	private Country getCountry(String country) {
		Country c = null;
		try {
			c = CountryDAO.getInstance().getCountryByCode(country);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return c;
	}

}
