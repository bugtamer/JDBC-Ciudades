package com.github.bugtamer.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.github.bugtamer.database.CityDAO;
import com.github.bugtamer.database.CountryDAO;
import com.github.bugtamer.models.City;
import com.github.bugtamer.models.Country;

@WebServlet("/customCityQuery")
public class CustomCityQueryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		System.out.println("CustomCityQueryServletPath=" + request.getServletPath());

		String customCityQuery = getCustomCityQuery(request);
		request.setAttribute("path",            request.getServletPath());
		request.setAttribute("cities",          getCities(customCityQuery));
		request.setAttribute("countries",       getCountries());
		request.setAttribute("customCityQuery", customCityQuery);
		request.getRequestDispatcher("customCityQuery.jsp").forward(request, response);
	}

	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		request.setAttribute("customQuery", request.getParameter("query"));
		doGet(request, response);
	}



	// LOW LEVEL IMPLEMENTATION DETAILS
	
	private String getCustomCityQuery(HttpServletRequest request) {
		String customQuery   = (String) request.getAttribute("customQuery");
		String defaultQuery  = "SELECT *\nFROM city\nORDER BY Name ASC";
		return (customQuery == null) ? defaultQuery : customQuery;
	}
	

	private List<City> getCities(String query) {
		List<City> cities = null;
		try {
			cities = CityDAO.getInstance().getList(query);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cities;
	}


	private List<Country> getCountries() {
		List<Country> countries = null;
		try {
			countries = CountryDAO.getInstance().getList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return countries;
	}

}
