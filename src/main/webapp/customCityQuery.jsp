<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.github.bugtamer.models.City" %><%-- <w:cityList/> --%>
<%@ taglib prefix="w" tagdir="/WEB-INF/tags" %>
<html>
<head>
	<link rel="stylesheet" href="./css/world.css">
</head>
<body>
	<div id="container">
		<h1>World</h1>
		<h2>Cities</h2>
		<w:error/>
		<p>WARNING! only for: SELECT * FROM City</p>
		<w:customCityQuery/>
		<w:nav/>
		<w:cityList/>
	</div>
</body>
</html>
