<%@ taglib prefix="w" tagdir="/WEB-INF/tags" %>
<%@ page import="com.github.bugtamer.models.City" %>
<html>
<head>
	<link rel="stylesheet" href="./css/world.css">
</head>
<body>
	<div id="container">
		<h1>World</h1>
		<h2>New City and Country (Transactions)</h2>
		<w:nav/>

		<form action="./newCityCountry" method="POST">
			<h3>New city...</h3>
			<input name="name"        type="text"   value="Fake name"      placeholder="name" /><br>
			<input name="district"    type="text"   value="Fake district"  placeholder="district" /><br>
			<input name="population"  type="number" value="100"            placeholder="population"  /><br>
			<h3>...which belongs to this new Country</h3>
			<input name="countryCode" type="text"   value="AND"            placeholder="countryCode" /><br>
			<input name="countryName" type="text"   value="Andorra"        placeholder="country name" /><br>
			<button>New City and Country</button>
			<br>
		</form>
		<p class="error">${error}</p>
	</div>
</body>
</html>
